import { API_KEY } from '../common/constants.js';


/**
 * Does a get request to the server for the trending gif section
 * @author Yordan Yordanov <yordan.yordanov.a29@learn.telerikacademy.com>
 * @param {number} offset the offset parameter needed for paging the results
 * @return {promise} the response from the server
 */
export const loadTrending = (offset) => {
  return fetch(`http://api.giphy.com/v1/gifs/trending?api_key=${API_KEY}&limit=24&offset=${offset}`);
};


/**
 * Does a get request to find the searched gif
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {string} searchTerm The searched gif name
  * @param {number} offset the offset parameter needed for paging the results
 * @return {promise} the response from the server
 */
export const loadSearched = (searchTerm, offset) => {
  return fetch(`http://api.giphy.com/v1/gifs/search?api_key=${API_KEY}&q=${searchTerm}&limit=24&offset=${offset};`);
};


/**
 * Does a get request to the Giphy server
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {array} idsArray the array with the gifs ids
 * @return {promise} the response from the server
 */
export const loadGifsByIdEndPoint = (idsArray = []) => {
  return fetch(`http://api.giphy.com/v1/gifs?api_key=${API_KEY}&ids=${idsArray.join(',')}`);
};


/**
 * Does a post request to the server
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {object} uploadedFile the file which will be uploaded to the server
 * @return {promise} the response from the server
 */
export const loadPostReq = (uploadedFile) => {
  return fetch(`http://upload.giphy.com/v1/gifs?api_key=${API_KEY}&username=kirebel`, {
    method: 'post',
    body: uploadedFile,
  });
};


/**
 * Does a get request to the server for specific gif through id
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {string} gifId the id needed to load specific response from the server
 * @return {promise} the response from the server
 */
export const loadGifById = (gifId) => {
  return fetch(`
  http://api.giphy.com/v1/gifs/${gifId}?api_key=${API_KEY}`);
};


/**
 * Does a get request to the server for a random gif
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @return {promise} the response from the server
 */

export const loadRandomGif = () => {
  return fetch(`http://api.giphy.com/v1/gifs/random?api_key=${API_KEY}`);
};
