import {
  loadTrending,
  loadGifsByIdEndPoint,
  loadPostReq,
  loadGifById,
} from './requests-2.js';
import {
  CONTAINER_SELECTOR,
  UPLOADED_GIFS_CONTAINER_SELECTOR,
} from '../common/constants.js';
import { getFavorites } from './favorites/favorites.js';
import { toggleFavoriteStatus } from './favorites/favoritesEvent.js';
import {
  toTrendingView,
  toSearchView,
  toUploadView,
  showUploadedGifs,
  toFavoritesView,
  toGifDetailView,
} from './views-4.js';
import {
  q,
  getArrayOfGifsUrls,
  getItemsFromLocalStorage,
  uploadGifsIdsToLocalStorage,
  getDetailedGifData,
  closureOffsetHelper,
  getRandomGifData,
} from './helpers.js';


/**
 * Shows trending view and trending gifs to the user. Also working with the
 * promise from the loadTrending function and fill
 * the selected DOM tree element with the needed
 * data by involking the toTrendingView function from views
 * @author Yordan Yordanov <yordan.yordanov.a29@learn.telerikacademy.com>
 * @param {number} offset the offset parameter needed for paging the results
 */
export const renderTrending = (offset) => {
  const offsetChange = closureOffsetHelper(offset);

  loadTrending(offset)
      .then((res) => res.json())
      .then((r) => {
        q(CONTAINER_SELECTOR)
            .innerHTML = toTrendingView(
                getArrayOfGifsUrls(r.data), offsetChange);
      });
};


/**
 * Adding the searched gif info
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {array} gifData The data of gif
 * @param {string} searchedText Searched
  * @param {number} offset the offset parameter needed for paging the results
 */
export const renderSearchItems = (gifData, searchedText, offset) => {
  const offsetChange = closureOffsetHelper(offset);
  gifData
      .then((res) => res.json())
      .then((r) => {
        q(CONTAINER_SELECTOR).innerHTML =
        toSearchView(getArrayOfGifsUrls(r.data), searchedText, offsetChange);
      });
};


/**
 * Representing the favorite page, loading all of the gifs
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 */
export const renderFavorites = () => {
  q(CONTAINER_SELECTOR).innerHTML = toFavoritesView(getFavorites());
};

/**
 * Show Uploaded section. Also working with the
 * promise from the loadPostReq function and fill
 * the selected DOM tree element with the needed
 * data by involving the toUploadView function from views
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {number} offset the offset parameter needed for paging the results
 */
export const renderUploadView = () => {
  q(CONTAINER_SELECTOR).innerHTML = toUploadView();
  const myForm = document.getElementById('form');
  const inputFile = document.getElementById('input-file');

  myForm.addEventListener('submit', (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append('file', inputFile.files[0]);

    loadPostReq(formData)
        .then((res) => res.json())
        .then((res) => {
          if (!uploadGifsIdsToLocalStorage(res.data.id)) {
            alert('Successfully uploaded file!');
          }
        });
  });
};

/**
 * Show Uploaded files to the user. Also working with the
 * promise from the loadGifsByIdEndPoint function,
 * getIdsArrayFromLocalStorage function and fill the selected DOM tree element
 * with the needed data by invoking the showUploadedGifs function from views
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 */
export const renderUploadedFiles = () => {
  loadGifsByIdEndPoint(getItemsFromLocalStorage())
      .then((res) => res.json())
      .then((res) => {
        q(UPLOADED_GIFS_CONTAINER_SELECTOR).
            innerHTML = showUploadedGifs(getArrayOfGifsUrls(res.data));
      });
};


/**
 * Providing through post request the full information about the gif,
 * giving this information to toGifDetailView, also appending it to div
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {string} gifId The id of gif
 */
export const renderGifDetails = (gifId) => {
  loadGifById(gifId)
      .then((res) => res.json())
      .then((res) => {
        const gifData = getDetailedGifData(res);
        if (q(`div[data-gif-id = "${gifId}"]`) === null) {
          q(`div[data-id-details="${gifId}"]`).append(toGifDetailView(gifData));
        } else {
          q(`div[data-gif-id = "${gifId}"]`).remove();
        }
      });
};

/**
 * Shows the random gif picked up when the time for picking up a random gif is
 * up. The toggleFavoriteStatus function puts the random gift given by
 * the server in the favorites section working with the data from the get
 * request by getRandomGifData function
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {promise} gifData the promise returned for the get request from
 * the server by loadRandomGif function
 */
export const renderRandomGif = (gifData) => {
  gifData
      .then((res) => res.json())
      .then((r) => {
        toggleFavoriteStatus(getRandomGifData(r));
      });
};
