// VIEWS FUNCTIONs
import { renderFavoriteStatus } from './helpers.js';


const toOneGif = (gifUrl) => `
  <div class="single-gif"  data-id-details = "${gifUrl.id}">
    <img class="gif-image" src="${gifUrl.url}" data-id-details = "${gifUrl.id}">
    ${renderFavoriteStatus(gifUrl)}  
  </div>
`;


export const homeView = () => `
  <div class="home-title">
    Welcome to the world of gifs
  </div>

  <div class="home-gif">
    <img src="./images/home-gif.gif" class="home-gif">
  </div>
`;


export const toTrendingView = (gifsUrlArray, offset) => `
  <div class="content">
    ${gifsUrlArray.map(toOneGif).join('\n')}
  </div>
  <div class="paging-menu">
    <button class="page" number=0 >1</button>
    <button class="page" number=${offset()} >2</button>
    <button class="page" number=${offset()} >3</button>
    <button class="page" number=${offset()} >4</button>
    <button class="page" number=${offset()} >5</button>
    <button class="page" number=${offset()} >More</button>
  </div>
`;


export const toFavoritesView = (gifs) => `
  <div id="movies">
    <h1>Favorite gifs:</h1>
    <div class="content">
        ${gifs.map(toOneGif).join('\n') ||
  '<p>Add some gifs to favorites to see them here.</p>'}
    </div>
  </div>
`;


export const toSearchView = (gifs, searchTerm, offset) => `
  <div id="gifs">
    ${gifs.length > 0 ? `<h1>Gifs found for "${searchTerm}":</h1>` : ''}

    <div class="content">
      ${gifs.map(toOneGif).join('\n') ||
      `<p>Nothing found for '${searchTerm}'.</p>`}
    </div>
  </div>
  
  <div class="paging-menu">
    <button class="search-page" number=0 >1</button>
    <button class="search-page" number=${offset()}>2</button>
    <button class="search-page" number=${offset()}>3</button>
    <button class="search-page" number=${offset()}>4</button>
    <button class="search-page" number=${offset()} >5</button>
    <button class="search-page" number=${offset()} >More</button>
  </div>
`;

export const showUploadedGifs = (uploadedGifsUrls) =>
  uploadedGifsUrls.map(toOneGif).join('\n');


export const toUploadView = () => `
  <div class="to-upload">
    <span>Upload gif</span>
    <form id="form" class="upload-btn">
        <input type="file" id="input-file" class="hide-file">
        <button type="submit" id="btn" >Upload here</button>
    </form>
    <button class="viz">Visualize uploads</button>
    <div id ="uploaded"></div>
  </div>
`;


// Dynamically creating HTML and adding detailed gif information
export const toGifDetailView = (gifData) => {
  const gifId = gifData.id;

  const div = document.createElement('div');
  div.setAttribute('data-gif-id', gifData.id);
  div.classList.add('gif-info');

  div.innerHTML = `
    <div class="detailed-gif-text" data-gif-id="${gifId}">
        <span class="details-title"  data-gif-id="${gifId}">
          Author:
        </span>
        ${gifData.author ? gifData.author : 'No info available'}
    </div>

    <div class="detailed-gif-text" data-gif-id="${gifId}">
        <span class="details-title"  data-gif-id="${gifId}">
          Title:
        </span>
        ${gifData.title} 
    </div>

    <div class="detailed-gif-text" data-gif-id="${gifId}">
      <span class="details-title"  data-gif-id="${gifId}">
        Added Date:
      </span> 
        ${gifData.addedDate.split(' ')[0]}
    </div>`;

  return div;
};

