/* eslint-disable no-alert */
// eslint-disable-next-line max-len
import {
  renderTrending,
  renderSearchItems,
  renderUploadView,
  renderUploadedFiles,
  renderFavorites,
  renderGifDetails,
  renderRandomGif,
} from './navigation-3.js';
import { q } from './helpers.js';
import { loadRandomGif, loadSearched } from './requests-2.js';
import { CONTAINER_SELECTOR } from '../common/constants.js';
import { toggleFavoriteStatus } from './favorites/favoritesEvent.js';
import { getFavorites } from './favorites/favorites.js';
import { homeView } from './views-4.js';

document.addEventListener('DOMContentLoaded', () => {
  // add global listener

  //  Search with 'enter' key
  q('input#search').addEventListener('keyup', (e) => {
    const gifData = loadSearched(
        q('input#search').value,
        +e.target.getAttribute('number')
    );

    const searchedText = q('input#search').value;
    const offSet = +e.target.getAttribute('number');

    if (e.keyCode === 13) {
      e.preventDefault();
      renderSearchItems(gifData, searchedText, offSet);
    }
    q(CONTAINER_SELECTOR).classList.remove('main-page');
  });

  //  Search with added button
  q('button#search-button').addEventListener('click', (e) => {
    const gifData = loadSearched(
        q('input#search')
            .value, +e.target.getAttribute('number')
    );

    const searchedText = q('input#search').value;
    const offSet = +e.target.getAttribute('number');

    renderSearchItems(gifData, searchedText, offSet);
    q(CONTAINER_SELECTOR).classList.remove('main-page');
  });


  document.addEventListener('click', (e) => {
    // EVENT LISTENERs TO NAVIGATION EVENT


    // Toggle favorite events
    if (e.target.classList.contains('favorite')) {
      const gifUrl = e.target.getAttribute('data-gif-url');
      const gifId = e.target.getAttribute('data-gif-id');
      const gifData = { id: gifId, url: gifUrl };

      toggleFavoriteStatus(gifData);
    }
    // Visualize favorite page + removing class main-page
    if (e.target.classList.contains('favorites')) {
      renderFavorites();
      q(CONTAINER_SELECTOR).classList.remove('main-page');
    }

    // Visualize trending page + removing class main-page
    if (e.target.classList.contains('trending')) {
      renderTrending(+e.target.getAttribute('data-page'));
      q(CONTAINER_SELECTOR).classList.remove('main-page');
    }

    // Visualize trending gifs for paging + removing class main-page
    if (e.target.classList.contains('page')) {
      renderTrending(e.target.getAttribute('number'));
      q(CONTAINER_SELECTOR).classList.remove('main-page');
    }

    // Visualize home page + adding class main-page only there
    if (e.target.classList.contains('image')) {
      q(CONTAINER_SELECTOR).innerHTML = homeView();
      q(CONTAINER_SELECTOR).classList.add('main-page');
    }

    // Upload events + adding class main-page
    if (e.target.classList.contains('upload')) {
      renderUploadView(+e.target);
      q(CONTAINER_SELECTOR).classList.remove('main-page');
    }

    // Visualizing uploaded gifs + adding class main-page
    if (e.target.classList.contains('viz')) {
      renderUploadedFiles(+e.target);
      q(CONTAINER_SELECTOR).classList.remove('main-page');
    }

    // Visualizing gif details / specifying the click box
    if (e.target.hasAttribute('data-id-details') &&
      !e.target.classList.contains('single-gif')) {
      renderGifDetails(e.target.getAttribute('data-id-details'));
    }

    // Visualizing gif details / specifying the click box
    if (e.target.hasAttribute('data-gif-id') &&
      !e.target.hasAttribute('data-gif-url')) {
      renderGifDetails(e.target.getAttribute('data-gif-id'));
    }

    // Visualizing searched gifs for paging
    if (e.target.classList.contains('search-page')) {
      renderSearchItems(loadSearched(q('input#search').value,
          +e.target.getAttribute('number')),
      q('input#search').value,
      +e.target.getAttribute('number'));
      q(CONTAINER_SELECTOR).classList.remove('main-page');
    }
  });

  q(CONTAINER_SELECTOR).innerHTML = homeView();
});

// Adding random favorite gif if no added for 90 seconds
if (getFavorites().length === 0) {
  setTimeout(() => {
    alert(
        'You have not added anything to favorites, so we have done it for you.'
    );

    renderRandomGif(loadRandomGif());
  }, 90000);
}
