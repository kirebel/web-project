let favorites = JSON.parse(localStorage.getItem('favorites')) || [];


/**
 * Adding favorite gif to the local storage
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {object} gifData The data of gif containing id/
 */
export const addFavorite = (gifData) => {
  if (favorites.find((gif) => gif.id === gifData.id)) {
    return;
  }
  favorites.push(gifData);
  localStorage.setItem('favorites', JSON.stringify(favorites));
};


/**
 * Removing favorite gif from the local storage
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {object} gifData The data of gif containing id/
 */
export const removeFavorite = (gifData) => {
  favorites = favorites.filter((gif) => gif.id !== gifData.id);
  localStorage.setItem('favorites', JSON.stringify(favorites));
};

/**
 * Removing favorite from the local storage
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @return {array} Array with favorite gif containing data
 */
export const getFavorites = () => [...favorites];
