import { addFavorite, getFavorites, removeFavorite } from './favorites.js';
import { q, renderFavoriteStatus } from '../helpers.js';


/**
 * Function behind the favorite event, pushing/removing the gif from favorites,
 * also adding innerHTML to span
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {object} gifData The data of gif containing id/
 */
export const toggleFavoriteStatus = (gifData) => {
  const favorites = getFavorites();

  if (favorites.findIndex((gif) => gif.id === gifData.id) > -1) {
    removeFavorite(gifData);
  } else {
    addFavorite(gifData);
  }

  if (q(`span[data-gif-id="${gifData.id}"]`) !== null) {
    q(`span[data-gif-id="${gifData.id}"]`)
        .innerHTML = renderFavoriteStatus(gifData);
  }
};
