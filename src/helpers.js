import { FULL_HEART, EMPTY_HEART } from '../common/constants.js';
import { getFavorites } from './favorites/favorites.js';

/**
 * A function which returns the first element that matches
 * a specifield selector in the document.
 * @author Yordan Yordanov <yordan.yordanov.a29@learn.telerikacademy.com>
 * @param {String} Selector id in the document
 * @return {function} returns the first Element within the document,
 * that matches the specified selector, or group of selectors.
 */

export const q = (selector) => document.querySelector(selector);

/**
 * A function which returns all the elements that match
 * a specifield selector in the document.
 * @author Yordan Yordanov <yordan.yordanov.a29@learn.telerikacademy.com>
 * @param {String} Selector  id in the document
 * @return {function} returns a static NodeList representing
 *  a list of the document's elements that match the specified group
 *  of selectors.
 */

export const qs = (selector) => document.querySelectorAll(selector);


/**
 * Function for reaching url/id properties of array with gif data
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {array} gifData Array of objects(gif data) with properties
 * @return {object} returns an array with objects which
 * have id and url properties
 */
export const getArrayOfGifsUrls = (gifData) => {
  return gifData.map((gif) => ({ url: gif.images.downsized.url, id: gif.id }));
};


/**
 * Function for reaching title/author/addedDate/id properties of gif data
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {object} gifData The data of gif
 * @return {object} returns an object with title/author/addedDate/id properties
 */
export const getDetailedGifData = (gifData) => {
  return ({
    title: gifData.data.title,
    author: gifData.data.username,
    addedDate: gifData.data.import_datetime,
    id: gifData.data.id,
  });
};


/**
 * A function which purpose is to get items from the local storage
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {object} gifData an object with a lot of data
 * @return {object} returns an object with id and url properties
 * which are needed for the later functionality
 */
export const getRandomGifData = (gifData) => {
  return ({
    id: gifData.data.id,
    url: gifData.data.images.original.url,
  });
};


/**
 * Function which is changing the favorite status of a GIF,
 * also providing innerHTML for toggleFavoriteStatus func
 * @author Ivaylo Garnev <ivaylo.garnev.a29@learn.telerikacademy.com>
 * @param {object} gifData The data of gif containing id/url
 * @return {html} returns span element with off/on favorite class,
 * containing empty/full heart in it.
 */
export const renderFavoriteStatus = (gifData) => {
  const favorites = getFavorites();

  if (favorites.findIndex((gif) => gif.id === gifData.id) > -1) {
    return `<span class="favorite active" 
      data-gif-url="${gifData.url}" data-gif-id="${gifData.id}">
      ${FULL_HEART}</span>`;
  }
  return `<span class="favorite" data-gif-url="${gifData.url}"
  data-gif-id="${gifData.id}">
  ${EMPTY_HEART}</span>`;
};


/**
 * A function which purpose is to get items from the local storage
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @return {object} returns an object from the localStorage
 */
export const getItemsFromLocalStorage = () => {
  return JSON.parse(localStorage.getItem('gifIds'));
};


/**
 * A function which purpose is to create an array with ids of the uploaded gifs
 * and attach it to the local storage. IIFE used.
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @param {string} idAfterFetch the ID needed for the proper functionality
 */
export const uploadGifsIdsToLocalStorage = (idAfterFetch) => {
  const upload = JSON.parse(localStorage.getItem('gifIds')) || [];
  (function addUpload(localStorageArray, id) {
    localStorageArray.push(id);
    localStorage.setItem('gifIds', JSON.stringify(localStorageArray));
  }(upload, idAfterFetch));
};


/**
 * A function that set the helps setting the pagination of the results
 * in the search and trending functionality
 * @author Kiril Belchinski <kiril.belchinski.a29@learn.telerikacademy.com>
 * @return {closure} returns a closure of the
 * closureOffsetHelper which is used for pagination
 */
export const closureOffsetHelper = () => {
  let offset = 0;
  const off = () => {
    return offset += 20;
  };
  return off;
};
